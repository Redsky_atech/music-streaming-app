import { ad } from "utils/utils"
import { Injectable } from "@angular/core";
import { GlobalNotificationBuilder } from './global-notification-instance'

declare var android: any;
declare var cmom: any;
declare var com: any;

declare var services: any;
declare var NotificationActionHandler: any;

@Injectable()

export class NotificationService {

    CHANNEL_ID = 'CHANNLE_ID';

    constructor() {
    }

    postNotification(message: any) {

        var context = ad.getApplicationContext();
        var con = ad.getApplication();
        var notificationManager = context.getSystemService(android.content.Context.NOTIFICATION_SERVICE);

        var builder = new android.support.v4.app.NotificationCompat.Builder(context, 'NOTIFICATION');

        if (android.os.Build.VERSION.SDK_INT >= 26) {
            var importance = android.app.NotificationManager.IMPORTANCE_DEFAULT;
            var channel = new android.app.NotificationChannel('MUSIC_CHANNEL', 'Music_Channel', importance);

            notificationManager.createNotificationChannel(channel);
        }


        var mainIntent = new android.content.Intent(context, com.tns.NativeScriptActivity.class);


        var pendingIntent = android.app.PendingIntent.getActivity(context,
            1,
            mainIntent,
            android.app.PendingIntent.FLAG_UPDATE_CURRENT);

        var snoozeIntent = new android.content.Intent(context, cmom.tns.NotificationActionHandler.class);
        snoozeIntent.setAction("Pause");
        snoozeIntent.putExtra('NOTIFICATION-ID', 'NOTIFICATION')

        GlobalNotificationBuilder.setNotificationActionIntent(snoozeIntent);

        var snoozePendingIntent = android.app.PendingIntent.getBroadcast(con, 0, snoozeIntent, 0);

        builder.setContentTitle(message.subject)
            .setAutoCancel(true)
            .setContentText(message.title)
            .setVibrate([1000, 100, 1000]) // optional
            .setSmallIcon(context.getResources().getIdentifier("img_logo_splash",
                "drawable", context.getPackageName()))
            .setOnlyAlertOnce(true)
            .setOngoing(true)
            .setContentIntent(pendingIntent)
            .addAction(0, "Pause", snoozePendingIntent)

        GlobalNotificationBuilder.setNotificationCompatBuilderInstance(builder, pendingIntent, snoozePendingIntent, snoozeIntent);

        notificationManager.notify(888, builder.build());

    }
}