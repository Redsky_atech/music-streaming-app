import { Injectable } from "@angular/core";
import { TNSPlayer } from "nativescript-audio-ssi";
import { Subject } from "rxjs/internal/Subject";

export enum AdUnits {
    interstitial = 0,
    banner = 1
}

@Injectable()
export class SongService {
    player = new TNSPlayer();

    private _playerStateSubject = new Subject<boolean>();

    playerStateChanges = this._playerStateSubject.asObservable();


    constructor() { }


    setPlayer(player: TNSPlayer) {
        this.player = player;
    }

    getPlayer() {
        return this.player;
    }


    playerState(state: boolean) {
        this._playerStateSubject.next(state)
    }

    play() {
        this.player.play();
    }

    pause() {
        this.player.pause();
    }



}