import { configureTnsOAuth } from "nativescript-oauth2";
import { TnsOaProvider, TnsOaProviderOptionsFacebook, TnsOaProviderFacebook } from "nativescript-oauth2/providers";

export function configureOAuthProviders() {
    const facebookProvider = configureOAuthProviderFacebook();

    configureTnsOAuth([facebookProvider]);
}

export function configureOAuthProviderFacebook(): TnsOaProvider {
    const facebookProviderOptions: TnsOaProviderOptionsFacebook = {
        openIdSupport: "oid-none",
        clientId: "349645965835098",
        clientSecret: "257f0c879fb163d69664a6c57283007c",
        redirectUri: "https://www.facebook.com/connect/login_success.html",
        scopes: ["email"]
    };
    const facebookProvider = new TnsOaProviderFacebook(facebookProviderOptions);
    return facebookProvider;
}