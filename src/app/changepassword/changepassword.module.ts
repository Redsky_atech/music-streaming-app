import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { ChangePasswordRoutingModule } from "./changepassword-routing.module";
import { ChangePasswordComponent } from "./components/changepassword.component";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        ChangePasswordRoutingModule
    ],
    declarations: [
        ChangePasswordComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ChangePasswordModule { }
