import { Component, OnInit } from "@angular/core";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { RouterExtensions, ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { Values } from "~/app/values/values";
import { ActivatedRoute } from "@angular/router";
import { UserService } from "~/app/services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { User } from "~/app/models/user";

@Component({
    selector: "ChangePassword",
    moduleId: module.id,
    templateUrl: "./changepassword.component.html",
    styleUrls: ['./changepassword.component.css']
})
export class ChangePasswordComponent implements OnInit {

    border_color_current_password;
    border_color_new_password;
    border_color_confirm_password;
    background_color;
    textField: TextField
    user;
    currentPasswordText = '';
    newPasswordText = '';
    confirmNewPasswordText = '';

    isBusy: boolean = false;

    constructor(private routerExtensions: RouterExtensions, private activatedRoute: ActivatedRoute, private userService: UserService, private http: HttpClient) {

        this.userService.actionBarState(false)
        this.userService.homeSelector(false);

        this.border_color_current_password = 'orange';
        this.border_color_new_password = 'orange';
        this.border_color_confirm_password = 'orange';

        this.background_color = 'white';
        this.activatedRoute.queryParams.subscribe(params => {
            this.user = params["user"];
        })

        this.userService.userChanges.subscribe(user => {
            if (user == null || user == undefined) {

                let extendedNavigationExtras: ExtendedNavigationExtras = {
                    queryParams: {
                        "user": null
                    },
                };
                this.routerExtensions.navigate(["/home"], extendedNavigationExtras)
            }
        })
    }

    ngOnInit(): void {
    }

    onCurrentPasswordFocus() {
        this.border_color_current_password = 'red';
    }

    public onCurrentPasswordBlur() {
        this.border_color_current_password = 'orange';
    }

    onNewPasswordFocus() {
        this.border_color_new_password = 'red';
    }

    public onNewPasswordBlur() {
        this.border_color_new_password = 'orange';
    }

    onConfirmPasswordFocus() {
        this.border_color_confirm_password = 'red';
    }

    public onConfirmPasswordBlur() {
        this.border_color_confirm_password = 'orange';
    }

    public currentPasswordTextField(args) {
        var textField = <TextField>args.object;
        this.currentPasswordText = textField.text;
    }

    public newPasswordTextField(args) {
        var textField = <TextField>args.object;
        this.newPasswordText = textField.text;
    }

    public confirmNewPasswordTextField(args) {
        var textField = <TextField>args.object;
        this.confirmNewPasswordText = textField.text;
    }

    nativegateBack(): void {
        this.routerExtensions.back();
    }

    onLogout() {
        Values.writeString(Values.X_ROLE_KEY, "");
        this.routerExtensions.navigate(["/home"]);
    }

    onSubmitClick() {
        if (this.currentPasswordText == "") {
            alert("Current Password can not be empty");
            return;
        }
        if (this.newPasswordText == "") {
            alert("New Passsword can not be empty");
            return;
        }

        if (this.newPasswordText != this.confirmNewPasswordText) {
            alert("New Passwords do not match")
            return;
        }

        this.isBusy = true;
        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-tenant-code": "music",
            "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
        });

        this.user = new User();
        this.user.password = this.currentPasswordText;
        this.user.newPassword = this.newPasswordText;

        this.http.post("http://ems-api-dev.m-sas.com/api/users/resetPassword", this.user, { headers: headers }).subscribe((res: any) => {

            if (res.isSuccess) {
                let result: any
                result = res.data

                this.isBusy = false;
                let extendedNavigationExtras: ExtendedNavigationExtras = {
                    queryParams: {
                        "user": result
                    }
                };
                this.userService.setUser(result, Values.readString(Values.X_ROLE_KEY, ""));
                this.userService.homeSelector(true);
                this.routerExtensions.navigate(["/home"], extendedNavigationExtras);
            }
            else {
                console.log(res.error)
                this.isBusy = false;
            }
        },
            error => {
                console.log(error)
                this.isBusy = false;
            })
    }

}