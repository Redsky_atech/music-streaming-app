import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { AccountInfoComponent } from "~/app/accountinfo/components/accountinfo.component";
import { AccountInfoRoutingModule } from "./accountinfo-routing.module";
import { NgModalModule } from "../modal/ng-modal";
import { registerElement } from "nativescript-angular/element-registry";
import { NativeScriptUIGaugeModule } from "nativescript-ui-gauge/angular/gauges-directives";


registerElement("FilterableListpicker", () => require("nativescript-filterable-listpicker").FilterableListpicker);

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        AccountInfoRoutingModule,
        NgModalModule,
        NativeScriptUIGaugeModule
    ],
    declarations: [
        AccountInfoComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AccountInfoModule { }
