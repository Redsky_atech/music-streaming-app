import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef } from "@angular/core";
import { ActivatedRoute } from "@angular/router"
import { RouterExtensions } from "nativescript-angular/router";
import { Constants } from "../../models/constants";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Values } from "~/app/values/values";
import { Observable, EventData } from "tns-core-modules/data/observable";
import { UserService } from "~/app/services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { Song } from "~/app/models/song";
import { Page } from "tns-core-modules/ui/page/page";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { fromBase64, ImageSource, fromResource } from "tns-core-modules/image-source/image-source";
import { ActivityIndicator } from "tns-core-modules/ui/activity-indicator/activity-indicator";
import * as app from "application";

@Component({
    selector: "Favourites",
    moduleId: module.id,
    templateUrl: "./favourites.component.html",
    styleUrls: ['./favourites.component.css']
})

export class FavouritesComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('activity') activityRef: ElementRef;

    page: Page

    path: string;
    name: string = "Login";
    public status: boolean = false;
    size: number;
    data = [];
    source: Observable;
    songs = new ObservableArray();
    viewModel;
    user;
    refstatus: boolean;
    args;
    imagePlayer: string;
    imagePlayerFocussed: string;
    activity: ActivityIndicator;

    public tabSelectedIndex: number;
    public tabSelectedIndexResult: string;

    loggedIn: boolean = false;
    processing = false;

    public constant = new Constants();
    isRendering: boolean;
    renderViewTimeout;
    constructor(private activatedRoute: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient) {

        this.userService.actionBarState(true)
        this.userService.actionBarText('Favourites')
        this.userService.actionBarSearch(true);
        this.isRendering = false;

        this.activatedRoute.queryParams.subscribe(params => {
            this.user = params["user"]
        })

        if (this.user != null) {
            this.loggedIn = true;
        }
        else {
            if (Values.readString(Values.X_ROLE_KEY, "") != "") {
                this.loggedIn = true;
            }
            else {
                this.loggedIn = false;
            }
        }

        this.userService.userChanges.subscribe(user => {
            if (user == null || user == undefined) {

                let extendedNavigationExtras: ExtendedNavigationExtras = {
                    queryParams: {
                        "user": null
                    },
                };
                this.routerExtensions.navigate(["/home"], extendedNavigationExtras)
            }
        })
        this.imagePlayer = 'res://icon_video_play';
        this.imagePlayerFocussed = 'res://icon_video_play_hover';

    }

    pageLoaded(args: EventData) {
        this.page = args.object as Page;
        this.activity = this.activityRef.nativeElement as ActivityIndicator;


        // this.renderViewTimeout = setTimeout(() => {
        //     this.isRendering = true;
        // }, 500)

        this.getFavouriteSongs(Values.readString(Values.X_ROLE_KEY, ""));


    }


    pageUnloaded() {
        this.songs = new ObservableArray();
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
    }

    ngAfterViewInit(): void {
    }

    favouritesTab(args) {
        var pullRefresh = args.object;
        this.getFavouriteSongs(Values.readString(Values.X_ROLE_KEY, ""))
        if (this.refstatus == true) {
            pullRefresh.refreshing = false;
        }
    }

    getThumbnailSrc(data: string) {
        if (data != null && data != undefined) {
            let base64Data = data.split(',');
            if (base64Data.length == 2) {
                if (base64Data[1] != null && base64Data[1] != undefined) {
                    const imageSrc = fromBase64(base64Data[1]);
                    return imageSrc;
                }
                else {
                    const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
                    return imgFromResources;
                }
            } else {
                const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
                return imgFromResources;
            }

        }
        else {
            const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
            return imgFromResources;
        }
    }

    getFavouriteSongs(xRoleKey: string) {
        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-tenant-code": "music",
            "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
        });

        this.isRendering = false;

        this.activity.busy = true;

        this.http.get("http://docs-api-dev.m-sas.com/api/123/123/files?isFavourite=true", { headers: headers }).subscribe((res: any) => {

            if (res.isSuccess) {
                this.refstatus = true;

                if (res.items != undefined && res.items != null) {
                    for (var i = 0; i < res.items.length; i++) {
                        if (res.items[i].mimeType == "audio/mp3")
                            this.songs.push(new Song(<Song>res.items[i]))
                    }
                }

                this.viewModel = new Observable();
                this.viewModel.set("items", this.songs);
                this.page.bindingContext = this.viewModel;
                this.activity.busy = true;
                setTimeout(() => {
                    this.isRendering = true;
                }, 500)
            }
            else {
                console.log(res.error)
                this.activity.busy = true;
                this.refstatus = true;
                setTimeout(() => {
                    this.isRendering = true;
                }, 500)
                return null;
            }
        },
            error => {
                this.activity.busy = true;
                console.log(error);
                setTimeout(() => {
                    this.isRendering = true;
                }, 500)
                this.refstatus = true;
                return null;
            })
    }

    getUser(xRoleKey: string) {

        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-tenant-code": "music",
            "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
        });

        this.http.get("http://ems-api-dev.m-sas.com/api/users/my", { headers: headers }).subscribe((res: any) => {

            if (res.isSuccess) {
                let result: any
                result = res.data
                this.userService.setUser(result, xRoleKey);
            }
            else {
                console.log(res.error)
                return null;
            }
        },
            error => {
                console.log(error)
                return null;
            })
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    cardClicked(song: Song) {

        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "id": song.id,
                "name": song.name,
                "thumbnail": song.thumbnail,
                "url": song.url,
                "isFavourite": song.isFavourite,
                "views": song.views
            },
        };
        this.routerExtensions.navigate(["/detail"], extendedNavigationExtras)
    }

    ngOnDestroy(): void {
        clearTimeout(this.renderViewTimeout);
    }
}



