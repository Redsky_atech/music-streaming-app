import { Component, OnInit, OnDestroy, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router"
import { RouterExtensions } from "nativescript-angular/router";
import { Constants } from "../../models/constants";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Values } from "~/app/values/values";
import { Observable, EventData } from "tns-core-modules/data/observable";
import { UserService } from "~/app/services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as app from "application";
import { Folder } from "~/app/models/folder";
import { Page } from "tns-core-modules/ui/page/page";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { fromBase64, ImageSource, fromResource } from "tns-core-modules/image-source/image-source";


@Component({
    selector: "Categories",
    moduleId: module.id,
    templateUrl: "./categories.component.html",
    styleUrls: ['./categories.component.css']
})

export class CategoriesComponent implements OnInit, AfterViewInit, OnDestroy {

    page: Page
    path: string;
    name: string = "Login";
    public status: boolean = false;
    size: number;
    items = new ObservableArray();
    source: Observable;
    isBusy: boolean = false;
    user;

    imagePlayer: string;
    imagePlayerFocussed: string;

    categoryFolders: Folder[];
    viewModel;

    public tabSelectedIndex: number;
    public tabSelectedIndexResult: string;

    loggedIn: boolean = false;

    public constant = new Constants();
    activity;

    isRendering: boolean;
    renderViewTimeout;
    constructor(private activatedRoute: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient) {

        this.userService.actionBarState(true)
        this.userService.actionBarText('Categories')


        console.log("data:", this.items, this.categoryFolders)

        this.isRendering = false;

        this.activatedRoute.queryParams.subscribe(params => {
            this.user = params["user"]

            if (this.user != null) {
                this.loggedIn = true;
            }
            else {
                if (Values.readString(Values.X_ROLE_KEY, "") != "") {
                    this.loggedIn = true;
                }
                else {
                    this.loggedIn = false;
                }
            }
        })

        this.userService.userChanges.subscribe(user => {
            if (user == null || user == undefined) {

                let extendedNavigationExtras: ExtendedNavigationExtras = {
                    queryParams: {
                        "user": null
                    },
                };
                this.routerExtensions.navigate(["/home"], extendedNavigationExtras)
            }
        })

        this.imagePlayer = 'res://icon_video_play';
        this.imagePlayerFocussed = 'res://icon_video_play_hover';
    }

    ngOnInit(): void {

    }

    ngAfterViewInit(): void {

    }

    pageLoaded(args: EventData) {
        this.page = args.object as Page;
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        this.getCategoryFolders();
        }, 500)
        console.log("Page Loaded called")
    }

    getThumbnailSrc(data: string) {
        if (data != null && data != undefined) {
            let base64Data = data.split(',');
            if (base64Data.length == 2) {
                if (base64Data[1] != null && base64Data[1] != undefined) {
                    const imageSrc = fromBase64(base64Data[1]);
                    return imageSrc;
                }
                else {
                    const imgFromResources: ImageSource = <ImageSource>fromResource("folder_image");
                    return imgFromResources;
                }
            } else {
                const imgFromResources: ImageSource = <ImageSource>fromResource("folder_image");
                return imgFromResources;
            }
        }
        else {
            const imgFromResources: ImageSource = <ImageSource>fromResource("folder_image");
            return imgFromResources;
        }
    }

    getCategoryFolders(): any {
        this.isBusy = true;
        this.viewModel = new Observable();
        this.viewModel.set("busy", this.isBusy);
        this.isRendering = false;
        this.page.bindingContext = this.viewModel;
        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-tenant-code": "music",
            "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
        });

        this.http.get("http://docs-api-dev.m-sas.com/api/folders?isParent=true", { headers: headers }).subscribe((res: any) => {

            if (res.isSuccess) {
                this.isBusy = false;

                if (res.items != undefined && res.items != null) {
                    for (var i = 0; i < res.items.length; i++) {
                        this.items.push(new Folder(<Folder>res.items[i]))
                    }
                }

                this.viewModel = new Observable();
                this.viewModel.set("items", this.items);

                this.page.bindingContext = this.viewModel;
                this.renderViewTimeout = setTimeout(() => {
                    this.isRendering = true;
                }, 500)
            }
            else {
                this.isBusy = false;
                this.viewModel = new Observable();
                this.viewModel.set("busy", this.isBusy);

                this.page.bindingContext = this.viewModel;
                this.renderViewTimeout = setTimeout(() => {
                    this.isRendering = true;
                }, 500)
                return null;
            }
        },
            error => {
                this.isBusy = false;
                this.viewModel = new Observable();
                this.viewModel.set("busy", this.isBusy);

                this.page.bindingContext = this.viewModel;
                this.renderViewTimeout = setTimeout(() => {
                    this.isRendering = true;
                }, 500)
                return null;
            })
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }


    onCardClicked(folder) {
        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "id": folder.id,
                "name": folder.name,
                "thumbnail": folder.thumbnail,
            },
        };
        this.routerExtensions.navigate(["/categoryFiles"], extendedNavigationExtras)
    }

    ngOnDestroy(): void {
        clearTimeout(this.renderViewTimeout);

    }

}



