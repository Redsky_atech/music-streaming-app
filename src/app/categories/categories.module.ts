import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { CategoriesRoutingModule } from "./categories-routing.module";
import { CategoriesComponent } from "./components/categories.component";
import { GridViewModule } from "nativescript-grid-view/angular";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        CategoriesRoutingModule,
        GridViewModule,
    ],
    declarations: [
        CategoriesComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class CategoriesModule { }
