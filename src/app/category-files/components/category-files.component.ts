import { Component, OnInit, OnDestroy, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router"
import { RouterExtensions } from "nativescript-angular/router";
import { Constants } from "../../models/constants";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Observable, EventData } from "tns-core-modules/data/observable";
import { UserService } from "~/app/services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as app from "application";
import { Folder } from "tns-core-modules/file-system/file-system";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { Song } from "~/app/models/song";
import { Page } from "tns-core-modules/ui/page/page";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { fromBase64, ImageSource, fromResource } from "tns-core-modules/image-source/image-source";
import { Values } from "~/app/values/values";

@Component({
    selector: "CategoryFiles",
    moduleId: module.id,
    templateUrl: "./category-files.component.html",
    styleUrls: ['./category-files.component.css']
})

export class CategoryFilesComponent implements OnInit, AfterViewInit, OnDestroy {


    page: Page
    path: string;
    name: string = "Login";
    public status: boolean = false;
    size: number;
    source: Observable;
    songs = new ObservableArray();
    viewModel;
    isBusy: boolean = false;

    args;
    refstatus: boolean;

    imagePlayer: string;
    imagePlayerFocussed: string;

    public tabSelectedIndex: number;
    public tabSelectedIndexResult: string;

    loggedIn: boolean = false;

    public constant = new Constants();

    folderName;
    folderId;
    folderThumbnail;

    isRendering: boolean;
    renderViewTimeout;
    constructor(private activatedRoute: ActivatedRoute, private router: Router, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient) {
        this.userService.homeSelector(false)
        this.userService.actionBarSearch(true);
        this.userService.actionBarState(true)

        this.activatedRoute.queryParams.subscribe(params => {
            this.folderName = params.name;
            this.folderId = params.id;
            this.folderThumbnail = params.thumbnail;

            if (this.folderName != null && this.folderName != undefined && this.folderName != "") {
                this.userService.actionBarState(true);
                this.userService.actionBarText(this.folderName)
                this.loggedIn = true;
            }
            // if (this.folderId != null && this.folderId != undefined && this.folderId != "") {
            //     this.getCategoryFilesByFolder(this.folderId);
            // }



            this.userService.userChanges.subscribe(user => {
                if (user == null || user == undefined) {

                    let extendedNavigationExtras: ExtendedNavigationExtras = {
                        queryParams: {
                            "user": null
                        },
                    };
                    this.routerExtensions.navigate(["/home"], extendedNavigationExtras)
                }
            })

        })

        this.imagePlayer = 'res://icon_video_play';
        this.imagePlayerFocussed = 'res://icon_video_play_hover';

    }

    pageLoaded(args: EventData) {
        this.page = args.object as Page;
        this.isBusy = false;
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
            if (this.folderId != null && this.folderId != undefined && this.folderId != "") {
                this.getCategoryFilesByFolder(this.folderId);
            }
        }, 500)
     
    }

    ngOnInit(): void {

    }

    ngAfterViewInit(): void {
    }

    categoryfilesTab(args) {
        var pullRefresh = args.object;
        if (this.folderId != null && this.folderId != undefined && this.folderId != "") {
            this.getCategoryFilesByFolder(this.folderId);
            if (this.refstatus == true) {
                pullRefresh.refreshing = false;
            }
        }
    }

    getThumbnailSrc(data: string) {
        if (data != null && data != undefined) {
            let base64Data = data.split(',');
            if (base64Data.length == 2) {
                if (base64Data[1] != null && base64Data[1] != undefined) {
                    const imageSrc = fromBase64(base64Data[1]);
                    return imageSrc;
                }
                else {
                    const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
                    return imgFromResources;
                }
            } else {
                const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
                return imgFromResources;
            }

        }
        else {
            const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
            return imgFromResources;
        }
    }

    getCategoryFilesByFolder(folderId: string, folderName?: string): any {
        this.isBusy = true;
        this.isRendering = false;

        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-tenant-code": "music",
            "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
        });

        this.http.get("http://docs-api-dev.m-sas.com/api/123/123/files?folder-id=" + folderId, { headers: headers }).subscribe((res: any) => {

            if (res.isSuccess) {

                this.refstatus = true


                this.refstatus = true;
                this.isBusy = false;

                if (res.items != undefined && res.items != null) {
                    for (var i = 0; i < res.items.length; i++) {
                        if (res.items[i].mimeType == "audio/mp3")
                            this.songs.push(new Song(<Song>res.items[i]))
                    }
                }

                this.viewModel = new Observable();
                this.viewModel.set("items", this.songs);
                this.page.bindingContext = this.viewModel;

                let result: Folder[];
                result = <Folder[]>res.items;
                this.renderViewTimeout = setTimeout(() => {
                    this.isRendering = true;
                }, 500)
            }
            else {
                this.renderViewTimeout = setTimeout(() => {
                    this.isRendering = true;
                }, 500)
                this.refstatus == true;
                this.isBusy = false;
                return null;
            }
        },
            error => {
                console.log(error)
                this.refstatus == true;
                this.isBusy = false;
                return null;
            })
    }

    
    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onCardClicked(song: Song) {

        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "id": song.id,
                "name": song.name,
                "thumbnail": song.thumbnail,
                "url": song.url,
                "isFavourite": song.isFavourite,
                "views": song.views
            },
        };
        this.routerExtensions.navigate(["/detail"], extendedNavigationExtras)
    }

    ngOnDestroy(): void {
        clearTimeout(this.renderViewTimeout);

    }

}



