export class Folder {

    name: string;
    id: string;
    thumbnail: string;

    constructor(obj?: any) {
        if (!obj) {
            return;
        }

        this.name = obj.name;
        this.id = obj.id;
        this.thumbnail = obj.thumbnail;

    }
}