import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { RegisterRoutingModule } from "./register-routing.module";
import { RegisterComponent } from "./components/register.component";
import { NgModalModule } from "../modal/ng-modal";
import { NativeScriptCommonModule } from "nativescript-angular/common";


@NgModule({
    bootstrap: [
        RegisterComponent
    ],
    imports: [
        HttpModule,
        NativeScriptCommonModule,
        RegisterRoutingModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        NgModalModule
    ],
    declarations: [
        RegisterComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class RegisterModule { }
