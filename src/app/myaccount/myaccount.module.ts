import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { MyAccountComponent } from "~/app/myaccount/components/myaccount.component";
import { MyAccountRoutingModule } from "./myaccount-routing.module";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        MyAccountRoutingModule
    ],
    declarations: [
        MyAccountComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class MyAccountModule { }
