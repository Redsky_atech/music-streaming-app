import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { MyAccountComponent } from "./components/myaccount.component";


const routes: Routes = [
    { path: "", component: MyAccountComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class MyAccountRoutingModule { }
