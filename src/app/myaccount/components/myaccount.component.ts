import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { ActivatedRoute } from "@angular/router";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { UserService } from "~/app/services/user.service";
import * as app from "application";

@Component({
    selector: "MyAccount",
    moduleId: module.id,
    templateUrl: "./myaccount.component.html",
    styleUrls: ['./myaccount.component.css']
})
export class MyAccountComponent implements OnInit {

    user;
    isRendering: boolean;
    renderViewTimeout;
    constructor(private routerExtensions: RouterExtensions, private activatedRoute: ActivatedRoute, private userService: UserService) {

        this.userService.actionBarState(true)
        this.userService.actionBarText('My Account')
        this.isRendering = false;
        this.activatedRoute.queryParams.subscribe(params => {
            this.user = params["user"];
        })

        this.userService.userChanges.subscribe(user => {
            if (user == null || user == undefined) {

                let extendedNavigationExtras: ExtendedNavigationExtras = {
                    queryParams: {
                        "user": null
                    },
                };
                this.routerExtensions.navigate(["/home"], extendedNavigationExtras)
            }
        })
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
    }

    onAccountInfo() {
        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "user": this.user
            }
        };
        this.userService.homeSelector(false)
        this.routerExtensions.navigate(["/accountInfo"], extendedNavigationExtras);
    }

    onChangePassword() {
        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "user": this.user
            }
        };
        this.routerExtensions.navigate(["/changePassword"], extendedNavigationExtras);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    ngOnDestroy(): void {
        clearTimeout(this.renderViewTimeout);
    }

}