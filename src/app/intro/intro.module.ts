import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { HttpModule } from "@angular/http";

import { IntroComponent } from "./components/intro.component";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { IntroRoutingModule } from "~/app/intro/intro-routing.module";


@NgModule({
    bootstrap: [
        IntroComponent
    ],
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        HttpModule,
        NativeScriptHttpModule,
        IntroRoutingModule
    ],
    providers: [],
    declarations: [
        IntroComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class IntroModule { }