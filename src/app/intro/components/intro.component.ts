import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "tns-core-modules/ui/page/page";
import { UserService } from '~/app/services/user.service';


@Component({
  selector: "intro",
  moduleId: module.id,
  templateUrl: "./intro.component.html",
  styleUrls: ['./intro.component.css']
})
export class IntroComponent implements OnInit, AfterViewInit {
  @ViewChild('myCarousel') carouselRef: ElementRef;

  done: boolean = false;
  constructor(private page: Page, private routerExtensions: RouterExtensions, private userService: UserService) {
    this.userService.drawerSwipe(false);
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
  }

  myChangeEvent(args) {
    var changeEventText = 'Page changed to index: ' + args.index;
    if (args.index == 3) {
      this.done = true
    }
    console.log(changeEventText);
  }

  onFabTap(): void {
    this.routerExtensions.navigate(["/home"]);
  }

}