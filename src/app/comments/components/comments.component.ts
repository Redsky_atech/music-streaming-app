import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { Page, Observable, EventData } from "tns-core-modules/ui/page/page";
import { RouterExtensions, ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { Values } from "~/app/values/values";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { UserService } from "~/app/services/user.service";
import { User } from "~/app/models/user";
import { Profile } from "~/app/models/profile";
import { Address } from "~/app/models/address";
import { ActivatedRoute } from "@angular/router";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { screen } from "platform";
import { Comment } from "~/app/models/comment";
import * as application from "tns-core-modules/application";
import * as moment from 'moment';

@Component({
    selector: "Comments",
    moduleId: module.id,
    templateUrl: "./comments.component.html",
    styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
    @ViewChild('container') gridContainer: ElementRef;
    defaultImage: string;

    user;
    firstName = '';
    lastName = '';
    email = '';
    address = '';
    city = '';
    country = '';
    pinCode = '';
    phone = '';
    line1 = '';
    commentText = '';
    line2;
    postUser = new User();
    screenHeight = screen.mainScreen.heightDIPs;
    comments = new ObservableArray();
    songId;
    songName;
    songThumbnail;
    songUrl;
    songViews;
    songIsFavourite;
    viewModel;

    barHeight;
    layoutHeight;
    listHeight;
    bottomBarHeight;
    noComments: boolean;

    isRendering: boolean;
    renderViewTimeout;

    constructor(private page: Page, private routerExtensions: RouterExtensions, private http: HttpClient, private userService: UserService, private activatedRoute: ActivatedRoute) {

        this.userService.actionBarText("Comments")
        this.userService.actionBarSearch(false)
        this.defaultImage = "res://img_avatar";

        this.noComments = true;

        this.isRendering = false;
        application.android.on(application.AndroidApplication.activityBackPressedEvent, (args: any) => {
            args.cancel = true;
            this.barHeight = "10vh";
            this.layoutHeight = "90vh"
            this.listHeight = "70vh"
            this.bottomBarHeight = "20vh"
        });
        this.userService.actionBarState(false)

        this.barHeight = Math.floor(this.screenHeight * 0.08);
        this.layoutHeight = Math.floor(this.screenHeight * 0.92);
        this.listHeight = Math.floor(this.layoutHeight * 0.9)
        this.bottomBarHeight = Math.floor(this.layoutHeight * 0.1)

        this.activatedRoute.queryParams.subscribe(params => {
            this.songName = params.name;
            this.songId = params.id;
            this.songThumbnail = params.thumbnail;
            this.songUrl = params.url;
            this.songViews = params.views;
            this.songIsFavourite = <boolean>params.isFavourite;
        })
        if (Values.readString(Values.X_ROLE_KEY, "") != "" && Values.readString(Values.X_ROLE_KEY, "") != null && Values.readString(Values.X_ROLE_KEY, "") != undefined) {
            this.user = this.userCheck();

            if (this.user != null) {
                if (this.user.profile != null && this.user.profile != undefined) {
                    if (this.user.profile.firstName != null && this.user.profile.firstName != undefined) {
                        this.firstName = this.user.profile.firstName;
                    }
                    if (this.user.profile.lastName != null && this.user.profile.lastName != undefined) {
                        this.lastName = this.user.profile.lastName;
                    }
                }
                if (this.user.phone != null && this.user.phone != undefined) {
                    this.phone = this.user.phone;
                }
                if (this.user.email != null && this.user.email != undefined) {
                    this.email = this.user.email;
                }
                if (this.user.address != null && this.user.address != undefined) {
                    if (this.user.address.line1 != null && this.user.address.line1 != undefined) {
                        this.line1 = this.user.address.line1;
                    }
                    if (this.user.address.line2 != null && this.user.address.line2 != undefined) {
                        this.line2 = this.user.address.line2;
                    }
                    if (this.user.address.city != null && this.user.address.city != undefined) {
                        this.city = this.user.address.city;
                    }
                    if (this.user.address.country != null && this.user.address.country != undefined) {
                        this.country = this.user.address.country;
                    }
                    if (this.user.address.pinCode != null && this.user.address.pinCode != undefined) {
                        this.pinCode = this.user.address.pinCode;
                    }
                }

            }
        }
        else {
        }

        this.userService.userChanges.subscribe(user => {
            if (user == null || user == undefined) {

                let extendedNavigationExtras: ExtendedNavigationExtras = {
                    queryParams: {
                        "user": null
                    },
                };
                this.routerExtensions.navigate(["/home"], extendedNavigationExtras)
            }
        })
    }

    ngOnInit(): void {

    }

    pageLoaded(args: EventData) {
        this.page = args.object as Page;
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
        this.getComments();
    }


    pageUnloaded() {
        this.comments = new ObservableArray();
    }

    public firstNameTextField(args) {
        var textField = <TextField>args.object;
        this.firstName = textField.text;
    }

    public lastNameTextField(args) {
        var textField = <TextField>args.object;
        this.lastName = textField.text;
    }

    public emailTextField(args) {
        var textField = <TextField>args.object;
        this.email = textField.text;
    }
    public phoneTextField(args) {
        var textField = <TextField>args.object;
        this.phone = textField.text;
    }

    public addressTextField(args) {
        var textField = <TextField>args.object;
        this.address = textField.text;
    }
    public cityTextField(args) {
        var textField = <TextField>args.object;
        this.city = textField.text;
    }
    public pinTextField(args) {
        var textField = <TextField>args.object;
        this.pinCode = textField.text;
    }

    getComments() {
        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-tenant-code": "music",
            "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
        });


        this.http.get("http://rating-api-dev.m-sas.com/api/files/" + this.songId + "/ratingLogs", { headers: headers }).subscribe((res: any) => {

            if (res.isSuccess) {
                console.log("Ress:", res)
                let result: any
                result = res.items
                for (var i = 0; i < result.length; i++) {
                    this.comments.push(new Comment(result[i]))
                }
                if (this.comments.length != 0) {
                    this.noComments = false;
                }


                this.viewModel = new Observable();
                this.viewModel.set("items", this.comments);

                this.page.bindingContext = this.viewModel;
            }
            else {
            }
        },
            error => {
            })
    }

    onSendButton() {
        if (this.commentText == '' || this.commentText == undefined) {
            alert("Comment can not be empty")
            return;
        }

        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-tenant-code": "music",
            "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
        });

        var body = {
            "comments": [{
                "text": this.commentText,
                "date": moment.utc().format()
            }]
        }

        this.http.post("http://rating-api-dev.m-sas.com/api/files/" + this.songId + "/ratingLogs", body, { headers: headers }).subscribe((res: any) => {

            if (res.isSuccess) {
                let result: any
                result = res.data

                let extendedNavigationExtras: ExtendedNavigationExtras = {
                    queryParams: {
                        "user": null
                    }
                };
                this.routerExtensions.navigate(["/home"], extendedNavigationExtras);

            }
            else {
                console.log(res.error)
                return null;
            }
        },
            error => {
                console.log(error)
                return null;
            })
    }

    public commentTextField(args) {
        var textField = <TextField>args.object;
        this.commentText = textField.text;
    }

    public getCommentDate(item: any) {
        var date;
        var monthDate: number;
        if (item.comments.length != 0 && item.comments[0] != undefined && item.comments[0] != null) {
            if (item.comments[0].date != null && item.comments[0].date != "") {
                date = new Date(item.comments[0].date);
                monthDate = date.getMonth() + 1;
                return date.getDate() + "-" + monthDate + "-" + date.getUTCFullYear();
            }
            else {
                return "";
            }
        }
        else {
            return "";
        }
    }


    userCheck() {
        console.log(Values.readString(Values.X_ROLE_KEY, ""));
        return this.userService.currentUser;
    }

    onSubmit() {
        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-tenant-code": "music",
            "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
        });

        var profile = new Profile();
        profile.firstName = this.firstName;
        profile.lastName = this.lastName;


        var address = new Address();
        address.line1 = this.address;
        address.city = this.city;
        address.pinCode = this.pinCode;

        this.postUser.email = this.email;
        this.postUser.phone = this.phone;
        this.postUser.profile = profile;
        this.postUser.address = address;

        this.http.put("http://ems-api-dev.m-sas.com/api/users/my", this.postUser, { headers: headers }).subscribe((res: any) => {

            if (res.isSuccess) {
                let result: any
                result = res.data
                this.userService.setUser(result, Values.readString(Values.X_ROLE_KEY, ""));
                this.routerExtensions.navigate(["/home"]);
            }
            else {
                console.log(res.error)
                return null;
            }
        },
            error => {
                console.log(error)
                return null;
            })
    }

    nativegateBack(): void {
        this.routerExtensions.back();
    }

    ngOnDestroy(): void {
        clearTimeout(this.renderViewTimeout);

    }

}