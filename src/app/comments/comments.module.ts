import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { CommentsComponent } from "./components/comments.component";
import { CommentsRoutingModule } from "./comments-routing.module";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        CommentsRoutingModule,
    ],
    declarations: [
        CommentsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class CommentsModule { }
