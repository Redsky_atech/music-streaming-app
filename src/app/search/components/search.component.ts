import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router"
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from "../../services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ObservableArray } from "tns-core-modules/data/observable-array";
import { TokenModel } from "nativescript-ui-autocomplete";
import { Song } from "~/app/models/song";
import { EventData, Page, Observable } from "tns-core-modules/ui/page/page";
import { Values } from "~/app/values/values";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { ActivityIndicator } from "tns-core-modules/ui/activity-indicator/activity-indicator";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";


const countries = ["Albania", "Andorra", "Australia", "Belgium", "Bulgaria", "Cyprus", "Denmark", "Finland", "France", "Germany", "Greece", "Hungary", "Ireland", "Italy", "Japan", "Latvia", "Luxembourg", "Macedonia", "Moldova", "Monaco", "Netherlands", "Norway", "Poland", "Romania", "Russia", "Slovakia", "Slovenia", "Sweden", "Turkey", "Ukraine", "USA"];


@Component({
    selector: "search",
    moduleId: module.id,
    templateUrl: "./search.component.html",
    styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit, OnDestroy {
    @ViewChild('activity') activityRef: ElementRef;

    page;

    border_color;
    background_color;

    autocompleteCountries: ObservableArray<TokenModel>;
    songs = new ObservableArray();
    name = '';
    activity: ActivityIndicator;
    processing = false;
    viewModel: any;
    loggedIn: boolean = false;
    user;
    isRendering: boolean;
    renderViewTimeout;
    constructor(private activatedRoute: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient) {
        this.autocompleteCountries = new ObservableArray<TokenModel>();

        this.background_color = 'white';
        this.border_color = 'orange';
        this.isRendering = false;
        countries.forEach((country) => {
            this.autocompleteCountries.push(new TokenModel(country, undefined));
        });

        this.userService.actionBarState(false);
        this.userService.actionBarText("Search")

        this.activatedRoute.queryParams.subscribe(params => {
            this.user = params["user"]
        })

        if (this.user != null) {
            this.loggedIn = true;
        }
        else {
            if (Values.readString(Values.X_ROLE_KEY, "") != "") {
                this.loggedIn = true;
            }
            else {
                this.loggedIn = false;
            }
        }

        this.userService.userChanges.subscribe(user => {
            if (user == null || user == undefined) {

                let extendedNavigationExtras: ExtendedNavigationExtras = {
                    queryParams: {
                        "user": null
                    },
                };
                this.routerExtensions.navigate(["/home"], extendedNavigationExtras)
            }
        })
    }

    pageLoaded(args: EventData) {
        this.page = args.object as Page;
        this.activity = this.activityRef.nativeElement as ActivityIndicator;
        console.log("Page Loaded called")
    }

    pageUnloaded() {
        this.songs = new ObservableArray();
    }

    ngOnInit(): void {
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 500)
    }

    searchTextField(args) {
        var textField = <TextField>args.object;
        this.name = textField.text;
    }

    onSearchClick() {
        this.getSongByName(Values.readString(Values.X_ROLE_KEY, ""));
    }

    nativegateBack(): void {
        this.routerExtensions.back();
    }

    getSongByName(xRoleKey: string) {
        this.setProcessing();
        this.songs = new ObservableArray();

        console.log('processing before server call', this.processing)
        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-tenant-code": "music",
            "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
        });

        this.isRendering = false;

        this.http.get("http://docs-api-dev.m-sas.com/api/123/123/files?name=" + this.name, { headers: headers }).subscribe((res: any) => {

            if (res.isSuccess) {

                if (res.items != undefined && res.items != null) {
                    for (var i = 0; i < res.items.length; i++) {
                        if (res.items[i].mimeType == "audio/mp3")
                            this.songs.push(new Song(<Song>res.items[i]))
                    }
                }

                this.viewModel = new Observable();
                this.viewModel.set("items", this.songs);

                this.page.bindingContext = this.viewModel;

                this.renderViewTimeout = setTimeout(() => {
                    this.isRendering = true;
                }, 500)

                this.setProcessing();
                console.log('processing after server call', this.processing)
            }
            else {
                this.setProcessing();
                this.renderViewTimeout = setTimeout(() => {
                    this.isRendering = true;
                }, 500)
                console.log('processing after server call', this.processing)
                console.log(res.error)
                return null;
            }
        },
            error => {
                this.processing = false;
                this.renderViewTimeout = setTimeout(() => {
                    this.isRendering = true;
                }, 500)
                console.log('processing after server call', this.processing)
                console.log(error)
                return null;
            })
    }

    loading(args) {
        console.log(args)
    }

    setProcessing() {
        this.processing = !this.processing;
        this.activity.busy = this.processing;
    }

    ngOnDestroy(): void {
        this.songs = new ObservableArray();
        clearTimeout(this.renderViewTimeout);

    }

}

