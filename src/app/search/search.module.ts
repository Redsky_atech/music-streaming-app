import { SearchComponent } from "./components/search.component";
import { NO_ERRORS_SCHEMA, NgModule } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { SearchRoutingModule } from "./search-routing.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { GridViewModule } from "nativescript-grid-view/angular";


@NgModule({
    imports: [
        SearchRoutingModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        GridViewModule],
    declarations: [
        SearchComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SearchModule { }
