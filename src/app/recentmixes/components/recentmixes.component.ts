import { Component, OnInit, OnChanges, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { ActivatedRoute } from "@angular/router"
import { RouterExtensions } from "nativescript-angular/router";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Values } from "~/app/values/values";
import { Observable, EventData } from "tns-core-modules/data/observable";
import { UserService } from "~/app/services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Page } from "tns-core-modules/ui/page/page";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { Song } from "~/app/models/song";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { fromBase64, ImageSource, fromResource } from "tns-core-modules/image-source/image-source";
import { ActivityIndicator } from "tns-core-modules/ui/activity-indicator/activity-indicator";
import * as app from "application";

@Component({
    selector: "RecentMixes",
    moduleId: module.id,
    templateUrl: "./recentmixes.component.html",
    styleUrls: ['./recentmixes.component.css']
})

export class RecentMixesComponent implements OnInit, OnChanges, OnDestroy {
    @ViewChild('activity') activityRef: ElementRef;

    page: Page

    user;
    args;
    loggedIn: boolean = false;

    refstatus: boolean;
    songs = new ObservableArray();
    viewModel;
    isBusy: boolean = false;
    activity: ActivityIndicator;
    isRendering: boolean;
    renderViewTimeout;
    constructor(private activatedRoute: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient) {

        this.userService.actionBarState(true)
        this.userService.actionBarText('Recent Mixes')
        this.userService.actionBarSearch(true);

        this.activatedRoute.queryParams.subscribe(params => {
            this.user = params["user"]
        })

        if (this.user != null) {
            this.loggedIn = true;
        }
        else {
            if (Values.readString(Values.X_ROLE_KEY, "") != "") {
                this.loggedIn = true;
            }
            else {
                this.loggedIn = false;
            }
        }

        this.userService.userChanges.subscribe(user => {
            if (user == null || user == undefined) {
                this.loggedIn = false;
            }
        })

        this.userService.homeUpdation.subscribe(data => {
        }
        );

        this.isRendering = false;
    }

    ngOnInit(): void {

    }

    ngOnChanges(): void {
    }

    pageLoaded(args: EventData) {
        this.page = args.object as Page;
        this.activity = this.activityRef.nativeElement as ActivityIndicator;
        // this.renderViewTimeout = setTimeout(() => {
        //     this.isRendering = true;
        // }, 500)
        this.getRecentSongs();
    }

    pageUnloaded() {
        this.songs = new ObservableArray();
    }

    recentMixesTab(args) {
        var pullRefresh = args.object;
        this.getRecentSongs();

        if (this.refstatus == true) {
            pullRefresh.refreshing = false;
        }
    }

    getThumbnailSrc(data: string) {
        if (data != null && data != undefined) {
            let base64Data = data.split(',');
            if (base64Data.length == 2) {
                if (base64Data[1] != null && base64Data[1] != undefined) {
                    const imageSrc = fromBase64(base64Data[1]);
                    return imageSrc;
                }
                else {
                    const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
                    return imgFromResources;
                }
            } else {
                const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
                return imgFromResources;
            }

        }
        else {
            const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
            return imgFromResources;
        }
    }

    getRecentSongs() {
        // "x-role-key": "af769f88-ced0-3a71-5b96-c5120b8bd43d"

        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-tenant-code": "music",
            "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
        });

        this.activity.busy = true;

        this.isBusy = true;
        this.isRendering = false;
        this.http.get("http://docs-api-dev.m-sas.com/api/123/123/files?isRecent=true", { headers: headers }).subscribe((res: any) => {

            if (res.isSuccess) {
                this.refstatus = true;
                this.isBusy = true;

                if (res.items != undefined && res.items != null) {
                    for (var i = 0; i < res.items.length; i++) {
                        if (res.items[i].mimeType == "audio/mp3")
                            this.songs.push(new Song(<Song>res.items[i]))
                    }
                }

                this.viewModel = new Observable();
                this.viewModel.set("items", this.songs);

                this.page.bindingContext = this.viewModel;
                this.renderViewTimeout = setTimeout(() => {
                    this.isRendering = true;
                }, 500)
                this.activity.busy = false;

            }
            else {
                console.log(res.error)
                this.activity.busy = false;
                this.renderViewTimeout = setTimeout(() => {
                    this.isRendering = true;
                }, 500)
                this.refstatus = true;
                return null;
            }
        },
            error => {
                this.activity.busy = false;
                this.renderViewTimeout = setTimeout(() => {
                    this.isRendering = true;
                }, 500)
                console.log(error)
                this.refstatus = true;
                return null;
            })
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    cardClicked(song: Song) {

        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "id": song.id,
                "name": song.name,
                "thumbnail": song.thumbnail,
                "url": song.url,
                "isFavourite": song.isFavourite,
                "views": song.views
            },
        };
        this.routerExtensions.navigate(["/detail"], extendedNavigationExtras)
    }

    ngOnDestroy() {
        clearTimeout(this.renderViewTimeout);

    }
}



