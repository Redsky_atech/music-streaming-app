import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { RecentMixesRoutingModule } from "./recentmixes-routing.module";
import { RecentMixesComponent } from "./components/recentmixes.component";
import { GridViewModule } from "nativescript-grid-view/angular";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        GridViewModule,
        RecentMixesRoutingModule
    ],
    declarations: [
        RecentMixesComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class RecentMixesModule { }
